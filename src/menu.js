import React from 'react'
import {Link} from 'react-router-dom'
import ShowOrLog from './showOrLog'

const Menu = () => (
    <ShowOrLog>
        <div className="menu vertical-flex">
            <Link to="/write-english-to-russian">
                <div className="item">Write - English to Russian</div>
            </Link>
            <Link to="/write-russian-to-english">
                <div className="item">Write - Russian to English</div>
            </Link>
            <Link to="/select-english-to-russian">
                <div className="item">Select - English to Russian</div>
            </Link>
            <Link to="/select-russian-to-english">
                <div className="item">Select - Russian to English</div>
            </Link>
        </div>
    </ShowOrLog>
)

export default Menu
