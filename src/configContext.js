import React from 'react'

const ConfigContext = React.createContext()

function ConfigContextProvider({children}){

    const values = {
        rusovUrl : "http://localhost:7000",
        wordsBatch : 10,
        serverErrorMessage :  "Error connecting to server",
        requestOptions : { mode: 'cors', credentials : 'include' },
        apps : {
            englishToRussian       : 1,
            russianToEnglish       : 2,
            selectEnglishToRussian : 3,
            selectRussianToEnglish : 4
        },
        enums : {
            appStatus : {
                notLogged : 10,
                logged    : 20
            }
        }
    }

    return (
        <ConfigContext.Provider value={ values }>
            {children}
        </ConfigContext.Provider>
    )

}

export { ConfigContextProvider, ConfigContext }
