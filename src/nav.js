import { React, useContext } from 'react'
import { ConfigContext } from './configContext'
import {Link} from 'react-router-dom'

const Nav = () => {

    const { requestOptions, rusovUrl } = useContext(ConfigContext)

    const logout = () => {
        fetch(`${rusovUrl}/logout`, requestOptions)
            .then( response => response.json() )
            .then( response => {
                window.location = "/"
            })
    }

    return (
        <div className="nav horizontal-flex">
            <Link to="/"><div className="back"></div></Link>
            <div className="settings"></div>
            <Link to="/user-settings">
                <div className="user">
                </div>
            </Link>
        </div>
    )
}

export default Nav
