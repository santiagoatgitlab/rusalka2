import {React, useState} from 'react'
import {Route, Switch, useLocation} from 'react-router-dom'

import Header from './header.js'
import Nav from './nav.js'
import Menu from './menu.js'
import EngToRus from './engToRus.js'
import RusToEng from './rusToEng.js'
import SEngToRus from './sEngToRus.js'
import SRusToEng from './sRusToEng.js'
import UserSettings from './userSettings.js'
import Footer from './footer.js'

const App = () => {
    const [ program, setProgram ] = useState()
    const location = useLocation()
    if (location.pathname === '/' && program !== '') {
        setProgram('')
    }
    return (
        <div className="app vertical-flex">
            <div className="vertical-flex">
                <Header />
                <Nav />
                <p>{program}</p>
            </div>
            <Switch>
                <Route exact path="/"><Menu /></Route>
                <Route path="/write-english-to-russian"><EngToRus setProgram={setProgram}/></Route>
                <Route path="/write-russian-to-english"><RusToEng setProgram={setProgram} /></Route>
                <Route path="/select-english-to-russian"><SEngToRus setProgram={setProgram}/></Route>
                <Route path="/select-russian-to-english"><SRusToEng setProgram={setProgram} /></Route>
                <Route path="/user-settings"><UserSettings /></Route>
            </Switch>
            <Footer />
        </div>
    )
}
export default App
