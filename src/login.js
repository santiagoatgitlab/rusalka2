import { useState, useRef, useEffect, useContext } from 'react'
import { ConfigContext } from './configContext'

const Login = () => {

    const [error, setError]                 = useState(<></>)
    const [username, setUsername]           = useState('')
    const [password, setPassword]           = useState('')
    const [buttonEnabled, setButtonEnabled] = useState('')

    const { requestOptions, rusovUrl, enums } = useContext(ConfigContext)

    const handleChange = e => {
        switch (e.target.name) {
            case 'username' : setUsername(e.target.value); break
            case 'password' : setPassword(e.target.value); break
            default         : break;
        }
    }

    const usernameRef = useRef(null)

    const handleSend = e => {
        e.preventDefault()
        requestOptions.method = 'POST'
        requestOptions.body   = JSON.stringify({ username, password })
        fetch(`${rusovUrl}/login`, requestOptions)
            .then( response => response.json() )
            .then( response => {
                if (response.status.code == enums.appStatus.logged){
                    window.location = "/"
                }
                else{
                    setError(<div>Username or password incorrect</div>)
                }
            })
    }

    useEffect(() => {
        usernameRef.current.focus()
    }, [])

    useEffect(() => {
        if (password.length > 5 && username.length > 0){
            setButtonEnabled(true)
        }
        else{
            setButtonEnabled(false)
        }
    }, [username, password])

    return (
        <form className="login-form vertical-flex">
            <input
                type="text"
                name="username"
                value={username}
                placeHolder="username"
                onChange={handleChange}
                ref={usernameRef}
            />
            <input
                type="password"
                name="password"
                value={password}
                placeHolder="password"
                onChange={handleChange}
            />
            <button
                onClick={handleSend}
                disabled={!buttonEnabled}
            >Login</button>
            {error}
        </form>
    )
}

export default Login
