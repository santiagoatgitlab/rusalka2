import { React, useContext } from 'react'
import { ConfigContext } from './configContext'
import ShowOrLog from './showOrLog'
import Selector from './selector.js'

const SRusToEng = ({setProgram}) => {
    const {apps} = useContext(ConfigContext)
    setProgram('Select russian to english')
    const app = apps.selectRussianToEnglish
    return (
        <ShowOrLog>
            <Selector app={app} />
        </ShowOrLog>
    )
}

export default SRusToEng

