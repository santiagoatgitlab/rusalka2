import { React, useState, useContext, useEffect } from 'react'
import { ConfigContext } from './configContext.js'
import ShowOrLoading from './showOrLoading.js'

const Selector = ({app}) => {

    const states ={
        WAITING  : 0, // nothing happened yet
        CRITICAL : 1, // user committed one mistake
        PANIC    : 2, // user committed two mistakes and lost
        HAPPINES : 3  // user selected the right word
    }

    const getWordsUrl   = 'get_next_selection_words'
    const sendResultUrl = 'update_word_priority'
    const { rusovUrl, requestOptions } = useContext(ConfigContext)

    const [word, setWord]                     = useState({})
    const [state_, setState_]                 = useState(states.WAITING)
    const [ stats, setStats ]                 = useState([])
    const [loading, setLoading]               = useState(true)
    const [options, setOptions]               = useState([])
    const [message, setMessage]               = useState(<></>)
    const [mistakes, setMistakes]             = useState(0)
    const [selectedWordId, setSelectedWordId] = useState(0)
    const [optionsDisplay, setOptionsDisplay] = useState(<></>)

    function requestWords(){
        setLoading(true)
        requestOptions.method = 'GET'
        requestOptions.body = null
        fetch(`${rusovUrl}/${getWordsUrl}/${app}`, requestOptions)
            .then ( response => response.json() )
            .then ( response => {
                setWord(response.word)
                setOptions(response.options)
                setStats(response.stats)
            })
    }

    const handleClick = () => {
        if (selectedWordId === word.id){
            setMessage(<div className="message good">Good!</div>)
            updateWordState('good')
            setState_(states.HAPPINES)
        }
        else{
            setMessage(<div className="message wrong">Wrong!</div>)
            updateWordState('bad')
            setState_( currentState => currentState + 1 )
        }
    }

    const sendResult = () => {
        requestOptions.method = 'POST'
        requestOptions.body   = JSON.stringify({
            app,
            wordId:word.id,
            wrongs:mistakes,
            program:app
        })
        fetch(`${rusovUrl}/${sendResultUrl}`, requestOptions)
    }

    const updateWordState = newState => {
        setOptions( oldOptions => oldOptions.map(option => {
            if (option.id === selectedWordId){
                option.state = newState
            }
            return option
        }))
    }

    const reset = () => {
        setState_(states.WAITING)
        setMessage(<></>)
        setSelectedWordId(0)
        setMistakes(0)
        setOptionsDisplay([])
        requestWords()
    }

    useEffect(()=>{
        requestWords()
    }, [])

    useEffect(() => {
        let alreadySelected = options.some( option => { 
            return option.state !== 'clean' && option.id === selectedWordId 
        })
        if (selectedWordId > 0 && !alreadySelected){
            handleClick()
        }
    }, [selectedWordId])

    useEffect( () => {
        if (state_ < states.HAPPINES){
            setMistakes(state_)
        }
        else{
            sendResult()
        }
    }, [state_]) 

    useEffect(() => {
        if (mistakes === states.PANIC){
            sendResult()
            setOptions( options.filter( option => { 
                return option.state != 'clean'
                    || option.id == word.id
            }))
        }
    }, [mistakes])

    useEffect( () => {
        setOptionsDisplay( options.map( option => {
            return <div
                        key={option.id}
                        className={option.state}
                        onClick={() => setSelectedWordId(option.id)}
                   >{option.word}</div>
        }))
    }, [options])

    useEffect( () => {
        if (optionsDisplay.length > 0){
            setLoading(false)
        }
    }, [optionsDisplay])

    return (
        <ShowOrLoading loading={loading}>
            <div className="selector">
                <div className="word">{word.word}</div>
                <div className="options">{optionsDisplay}</div>
                {message}
                { state_ > states.CRITICAL 
                    ? <input type="button" value="next" onClick={reset} />
                    : <></>
                }
            </div>
            <div className="stats margin-top">
                <span>
                    { stats.total } { stats.sessionTotal } { stats.sessionGood }
                </span>
            </div>
        </ShowOrLoading>
    )
}

export default Selector
