import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom'
import {ConfigContextProvider} from './configContext'

import App from './App';

ReactDOM.render(
    <ConfigContextProvider>
        <Router>
            <App />
        </Router>
    </ConfigContextProvider>,
    document.getElementById('root')
);
