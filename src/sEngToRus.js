import { React, useContext } from 'react'
import { ConfigContext } from './configContext'
import ShowOrLog from './showOrLog'
import Selector from './selector.js'

const SEngToRus = ({setProgram}) => {
    const {apps} = useContext(ConfigContext)
    setProgram('Select english to russian')
    const app = apps.selectEnglishToRussian
    return (
        <ShowOrLog>
            <Selector app={app} />
        </ShowOrLog>
    )
}

export default SEngToRus
